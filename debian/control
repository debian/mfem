Source: mfem
Section: libs
Priority: optional
Maintainer: Alex Myczko <tar@debian.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
 cmake,
 chrpath,
 libmetis-dev,
 libhypre-dev,
 libunwind-dev,
 picojson-dev,
Standards-Version: 4.7.0
Homepage: https://github.com/mfem/mfem
Vcs-Browser: https://salsa.debian.org/debian/mfem
Vcs-Git: https://salsa.debian.org/debian/mfem.git
Rules-Requires-Root: no

Package: libmfem4.7
Provides: ${t64:Provides}
Replaces: libmfem4.7
#Breaks: libmfem4.7 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Lightweight, general, scalable C++ library for finite element methods
 This is a modular parallel C++ library for finite element methods. Its goal is
 to enable high-performance scalable finite element discretization research and
 application development on a wide variety of platforms, ranging from laptops
 to supercomputers.

Package: libmfem-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libmfem4.7 (= ${binary:Version}),
 libmetis-dev,
 libhypre-dev,
 libunwind-dev,
 picojson-dev,
 ${shlibs:Depends},
 ${misc:Depends},
Description: C++ library for finite element methods - development files
 This is a modular parallel C++ library for finite element methods. Its goal is
 to enable high-performance scalable finite element discretization research and
 application development on a wide variety of platforms, ranging from laptops
 to supercomputers.
 .
 This package provides mfem library development files.
